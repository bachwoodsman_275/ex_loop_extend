var arrBai1 = [];
// Chức năng thêm số khi nhân button Thêm
var inputClick = document.querySelector("#themSo");
inputClick.addEventListener("click", themSo);

// DOM tới ô input để lấy giá trị của ô input khi người dùng nhập
// Đặt biến để dùng biến inputSoDuong sau này
var inputSoDuong = document.querySelector("#soDuong");

// Đặt biến inputEnter để dùng cho việc lấy giá trị của ô input khi người dùng nhấn enter
var inputEnter = inputSoDuong;
inputEnter.addEventListener("keydown", function (event) {
    if (event.key === "Enter") {
        themSo();
    }
});

// Hàm thực hiện việc thêm số từ ô input vào mảng và in ra màn hình mảng, dùng để gọi
function themSo() {
    var soDuong = inputSoDuong.value * 1;
    arrBai1.push(soDuong);
    document.querySelector("#soDuong").value = "";
    var contentHTMLBai1_themSo = `<h4>Dãy số bạn nhập là: ${arrBai1}</h4>`;
    document.querySelector("#inRaDaySoNhap").innerHTML = contentHTMLBai1_themSo;
    return arrBai1;
}
var btnTinhTongCacSoDuong = document.querySelector("#tinhTongCacSoDuong");
btnTinhTongCacSoDuong.addEventListener("click", tinhTongSoDuong);

// Hàm thực hiện việc tính tổng các số dương và in ra màn hình
// Dùng for và if điều kiện
function tinhTongSoDuong() {
    var tongSoDuong = 0;
    for (var i = 0; i < arrBai1.length; i++) {
        if (arrBai1[i] >= 0) {
            tongSoDuong += arrBai1[i];
        }
    }
    var contentHTMLBai1_tinhTong = `<h4>Tổng của dãy số bạn nhập là: ${tongSoDuong}</h4>`;
    document.querySelector("#inRaTongDaySoNhap").innerHTML =
        contentHTMLBai1_tinhTong;
}

var btnDemSoDuong = document.querySelector("#demSoDuong");
btnDemSoDuong.addEventListener("click", demCacSoDuong);

// Hàm thực hiện việc đếm các số dương trong mảng
// Dùng for và biến count để đếm và if điều kiện
function demCacSoDuong() {
    var count = 0;
    var mangSoDuong = [];
    for (var i = 0; i < arrBai1.length; i++) {
        if (arrBai1[i] >= 0) {
            count++;
            var soDuong = arrBai1[i];
            mangSoDuong.push(soDuong);
        }
    }
    var contentHTMLBai1_demSoDuong = `<h4>Có ${count} số dương trong mảng là: ${mangSoDuong}</h4>`;
    document.querySelector("#inRaCacSoDuong").innerHTML =
        contentHTMLBai1_demSoDuong;
}

var btnTimSoNhoNhat = document.querySelector("#timSoNhoNhat");
btnTimSoNhoNhat.addEventListener("click", timSoNhoNhat);

// Hàm tìm số nhỏ nhất trong mảng
// Dùng for và if điều kiện so sánh phần tử đầu tiên so với các phần tử kế tiếp,số nhỏ hơn sẽ được được mang đi so sánh tiếp
function timSoNhoNhat() {
    var soNhoNhat = arrBai1[0];
    for (var i = 0; i < arrBai1.length; i++) {
        if (arrBai1[i] < soNhoNhat) {
            soNhoNhat = arrBai1[i];
        }
    }
    // 2 3 4 1
    var contentHTMLBai1_timSoNhoNhat = `<h4>Số ${soNhoNhat} là số nhỏ nhất trong mảng</h4>`;
    document.querySelector("#inRaSoNhoNhat").innerHTML =
        contentHTMLBai1_timSoNhoNhat;
}

var btnTimSoDuongNhoNhat = document.querySelector("#timSoDuongNhoNhat");
btnTimSoDuongNhoNhat.addEventListener("click", timSoDuongNhoNhat);

// Hàm tìm số dương nhỏ nhất trong mảng
// Dùng for và if điều kiện, gán soDuongNhoNhat = phần tử đầu tiên trong mảng và mang đi so sánh với các phần tử trong mảng, nếu phần tử trong mảng bé hơn số nhỏ nhất thì soDuongNhoNhat = phần tử trong mảng
function timSoDuongNhoNhat() {
    var soDuongNhoNhat = arrBai1[0];
    for (var i = 0; i < arrBai1.length; i++) {
        if (arrBai1[i] >= 0 && arrBai1[i] <= soDuongNhoNhat) {
            soDuongNhoNhat = arrBai1[i];
        }
    }
    var contentHTMLBai1_timSoDuongNhoNhat = `<h4>Số ${soDuongNhoNhat} là số dương nhỏ nhất trong mảng</h4>`;
    document.querySelector("#inRaSoDuongNhoNhat").innerHTML =
        contentHTMLBai1_timSoDuongNhoNhat;
}

var btnTimSoChanCuoiCung = document.querySelector("#soChanCuoiCung");
btnTimSoChanCuoiCung.addEventListener("click", inRaSoChanCuoiCung);

// Hàm tìm số chẵn cuối cùng trong mảng, nếu không có số chẵn thì trả về -1
// Dùng for và 2 if. Gán cho số chẵn bằng phần tử trong mảng nếu phần tử đó chia hết cho 2 , nếu không chia hết cho 2 thì soChan = notChan, nếu notChan = -1 và biến count = arrBai1.length thì in ra mảng không có phần tử chẵn, nếu biến count < arrBai1.length thì in ra số chẵn.
function timSoChanCuoiCung() {
    for (var i = arrBai1.length - 1; i >= 0; i--) {
        if (arrBai1[i] % 2 == 0) {
            return arrBai1[i];
        }
    }
    return -1;
}

function inRaSoChanCuoiCung() {
    var soChan = timSoChanCuoiCung();
    if (soChan == -1) {
        document.querySelector(
            "#inRaSoChanCuoiCung"
        ).innerHTML = `<h4>Không có số chẵn nào trong mảng</h4>`;
    } else {
        document.querySelector(
            "#inRaSoChanCuoiCung"
        ).innerHTML = `<h4>Số ${soChan} là số chẵn cuối cùng trong mảng</h4>`;
    }
}

var btnDoiViTri = document.querySelector("#doiViTri");
btnDoiViTri.addEventListener("click", doiViTri);

// Enter để nhập vị trí
// var enterNhapViTriDoi1 = document.querySelector("#viTriThuNhat").value * 1;
// enterNhapViTriDoi1.addEventListener("keydown", function (event) {
//     if (event.key === "Enter") {
//         event.defaultPrevented();
//     }
// });
// var enterNhapViTriDoi2 = document.querySelector("#viTriThuHai").value * 1;
// enterNhapViTriDoi2.addEventListener("keydown", function (event) {
//     if (event.key === "Enter") {
//         event.defaultPrevented();
//         doiViTri();
//     }
// });
// Lấy giá trị từ 2 vị trí người dùng nhập và hoàn đổi cho nhau

function doiViTri() {
    var viTriThuNhat = document.querySelector("#viTriThuNhat").value * 1;
    var viTriThuHai = document.querySelector("#viTriThuHai").value * 1;
    var temp = arrBai1[viTriThuNhat];
    arrBai1[viTriThuNhat] = arrBai1[viTriThuHai];
    arrBai1[viTriThuHai] = temp;
    console.log(arrBai1);
    document.querySelector("#inRaMangViTriMoi").innerHTML = `
            <h4>Mảng mới sau khi đổi vị trí là ${arrBai1}</h4>
            `;
}

var btnSapXepTangDan = document.querySelector("#sapXepTangDan");
btnSapXepTangDan.addEventListener("click", sapXepTangDan);

// dùng 2 vòng for
function sapXepTangDan() {
    var len = arrBai1.length;
    for (var i = 0; i < len; i++) {
        for (var j = 0; j < len - i - 1; j++) {
            if (arrBai1[j] > arrBai1[j + 1]) {
                var temp = arrBai1[j];
                arrBai1[j] = arrBai1[j + 1];
                arrBai1[j + 1] = temp;
            }
        }
    }
    document.querySelector("#inRaMangTangDan").innerHTML = `
            <h4>Mảng tăng dần là ${arrBai1}</h4>
            `;
}

var btnTimSoNguyenToDauTien = document.querySelector("#timSoNguyenToDauTien");
btnTimSoNguyenToDauTien.addEventListener("click", timSoNguyenToDauTien);

// For và if số chia hết cho 1 và chính nó thì return, nếu không có số nào thì trả về -1
function soNguyenTo(number) {
    for (var i = 2; i * i <= number; i++) {
        if (number % i == 0) {
            number = -1;
        } else {
            return number;
        }
    }
    return number;
}
function timSoNguyenToDauTien() {
    var arr = arrBai1;
    for (var i = 0; i < arr.length; i++) {
        var nguyenTo = soNguyenTo(arr[i]);
        if (nguyenTo == -1) {
            document.querySelector(
                "#inRaSoNguyenToDauTien"
            ).innerHTML = `<h4>Mảng không có số nguyên tố</h4>`;
        } else {
            document.querySelector(
                "#inRaSoNguyenToDauTien"
            ).innerHTML = `<h4>Số ${nguyenTo} là số nguyên tố đầu tiên</h4>`;
        }
    }
}

var btnThemSoThuc = document.querySelector("#themSoThuc");
btnThemSoThuc.addEventListener("click", themSo);

var soThucInput = document.querySelector("#soThuc");
soThucInput.addEventListener("keydown", function (event) {
    if (event.key === "Enter") {
        themSoThuc();
    }
});
// nhập thêm vào arrBai1 các số thực và đếm các số nguyên
function themSoThuc() {
    var soThuc = parseFloat(soThucInput.value);
    arrBai1.push(soThuc);
    document.querySelector("#soThuc").value = "";
    document.getElementById(
        "inRaSoThuc"
    ).innerHTML = `Mảng số thực của bạn là: ${arrBai1}`;
    return arrBai1;
}

var btnInRaSoNguyen = document.querySelector("#inRaMangSoNguyen");
btnInRaSoNguyen.addEventListener("click", demSoNguyen);

var arrSoNguyen = [];
function demSoNguyen() {
    var count = 0;
    var inRaSoNguyen = "";
    var arrSoThuc = themSoThuc();
    for (var i = 0; i < arrSoThuc.length; i++) {
        if (arrSoThuc[i] % 1 === 0) {
            arrSoNguyen.push(arrSoThuc[i]);
            count++;
        }
    }
    document.getElementById(
        "inRaSoThuc"
    ).innerHTML = `<p>Có ${count} số nguyên trong mảng là ${arrSoNguyen}</p>`;
}

var btnSoSanhAmDuong = document.getElementById("soSanhSoAmDuong");
btnSoSanhAmDuong.addEventListener("click", soSanhAmDuong);

function soSanhAmDuong() {
    var countSoAm = 0;
    var countSoDuong = 0;
    for (var i = 0; i < arrBai1.length; i++) {
        if (arrBai1[i] > 0) {
            countSoDuong++;
        } else if (arrBai1[i] < 0) {
            countSoAm++;
        }
    }
    var resultSoAm = `<h4>Số âm nhiều hơn</h4>`;
    var resultSoDuong = `<h4>Số dương nhiều hơn</h4>`;
    if (countSoDuong > countSoAm) {
        document.getElementById("inRaSoNhieuHon").innerHTML = resultSoDuong;
    } else if (countSoAm > countSoDuong) {
        document.getElementById("inRaSoNhieuHon").innerHTML = resultSoAm;
    }
}
